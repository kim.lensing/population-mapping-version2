# Map population to reference genome

## First follow the instructions here:
[Step by step guide on how to use my pipelines](https://carolinapb.github.io/2021-06-23-how-to-run-my-pipelines/)  
Click [here](https://github.com/CarolinaPB/snakemake-template/blob/master/Short%20introduction%20to%20Snakemake.pdf) for an introduction to Snakemake

#### Copy the pipeline:
Copy the pipeline to your directory, where you would run the pipeline.
```
cp -r /lustre/nobackup/WUR/ABGC/shared/pipelines_version2/population-mapping-version2/ <directory where you want to save it to>
```

### Install conda if it is not yet installed
See installation instructions below. 

#### Activate environmet:
Since the pipelines can take a while to run, it’s best if you use a screen session. By using a screen session, Snakemake stays “active” in the shell while it’s running, there’s no risk of the connection going down and Snakemake stopping. `screen -S <name of session>` to start a new screen session and `screen -r <name of session` to reattach to the screen session. In the screen session activate the conda environment.

```
conda activate /lustre/nobackup/WUR/ABGC/shared/pipelines_version2/envs/population-mapping-version2
```

#### Edit config.yaml with the paths to your files
Configure your paths, but keep the variable names that are already in the config file. See below for more details.

#### Run the pipeline on the cluster:
First test the pipeline with a dry run: `snakemake -np`. This will show you the steps and commands that will be executed. Check the commands and file names to see if there’s any mistake. If all looks ok, you can now run your pipeline


To run the pipeline, run the following command:
```
snakemake -j 8 --cluster-config cluster.yaml --cluster "sbatch --mem={cluster.mem} --time {cluster.time} --cpus-per-task {cluster.threads} --job-name={cluster.name} --output={cluster.output} --error={cluster.error}"

```


## ABOUT
This is a pipeline to map short reads from several individuals to a reference assembly. It outputs the mapped reads and a qualimap report.

#### Tools used:

- [Bwa-mem2](https://github.com/bwa-mem2/bwa-mem2) - mapping
- [Samtools](http://www.htslib.org/) - processing
- [Samblaster](https://github.com/GregoryFaust/samblaster) - marking duplicates
- [Qualimap](http://qualimap.conesalab.org/) - mapping summary

| ![DAG](/workflow.png)|
|:--:|
|*Pipeline workflow* |


### Edit config.yaml with the paths to your files
```
ASSEMBLY: /path/to/assembly
OUTDIR: /path/to/outdir
PATHS_WITH_FILES:
  path1: /path/to/dir
```

- ASSEMBLY - path to the assembly file
- OUTDIR - directory where snakemake will run and where the results will be written to.  
If you want the results to be written to this directory (not to a new directory), comment out ```OUTDIR: /path/to/outdir```
- PATHS_WITH_FILES - directory that can contain subdirectories where the fastq reads are located. You can add several paths by adding ```path2: /path/to/dir``` under ```PATHS_WITH_FILES```. (The line you add has to have indentation)

The script goes through the subdirectories of the directory you choose under ```PATHS_WITH_FILES``` looking for files with fastq extension.  
Example: if ```path1: /lustre/nobackup/WUR/ABGC/shared/Chicken/Africa/X201SC20031230-Z01-F006_multipath```, the subdirectory structure could be:  
```
/lustre/nobackup/WUR/ABGC/shared/Chicken/Africa/X201SC20031230-Z01-F006_multipath  
├── X201SC20031230-Z01-F006_1  
│   └── raw_data  
│       ├── a109_26_15_1_H  
│       │   ├── a109_26_15_1_H_FDSW202597655-1r_HWFFFDSXY_L3_1.fq.gz  
│       │   ├── a109_26_15_1_H_FDSW202597655-1r_HWFFFDSXY_L3_2.fq.gz  
│       │   └── MD5.txt  
│       └── a20_10_16_1_H  
│           ├── a20_10_16_1_H_FDSW202597566-1r_HWFFFDSXY_L3_1.fq.gz  
│           ├── a20_10_16_1_H_FDSW202597566-1r_HWFFFDSXY_L3_2.fq.gz  
│           └── MD5.txt  
└── X201SC20031230-Z01-F006_2  
    └── raw_data  
        ├── a349_Be_17_1_C  
        │   ├── a349_Be_17_1_C_FDSW202597895-1r_HWFFFDSXY_L3_1.fq.gz  
        │   ├── a349_Be_17_1_C_FDSW202597895-1r_HWFFFDSXY_L3_2.fq.gz  
        │   └── MD5.txt  
        └── a360_Be_05_1_H  
            ├── a360_Be_05_1_H_FDSW202597906-1r_HWFFFDSXY_L3_1.fq.gz  
            ├── a360_Be_05_1_H_FDSW202597906-1r_HWFFFDSXY_L3_2.fq.gz  
            └── MD5.txt  
```


## RESULTS
- **<run_date>_files.txt** dated file with an overview of the files used to run the pipeline (for documentation purposes)
- **processed_reads** directory with the bam files with the mapped reads for every sample
- **mapping_stats** directory containing the qualimap results and a summary of the qualimap results for all samples in ```sample_quality_summary.tsv```
  - **qualimap** contains qualimap results per sample

## Install conda (if it is not yet installed)
Install miniconda on linux. 
[Download installer.](https://docs.conda.io/en/latest/miniconda.html)
[Installation instructions.](https://conda.io/projects/conda/en/latest/user-guide/install/index.html)



1. Download the installer to your home directory. 
Choose the version according to your operating system. You can right click the link, copy and download with:
`wget <link>`

For example:
`wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh`



2. To install miniconda, run:
`bash <installer name>`

For example:
`bash  Miniconda3-latest-Linux-x86_64.sh`



3. Set up the conda channels in this order:
```
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
```


4. Activate the environment
```
conda activate /lustre/nobackup/WUR/ABGC/shared/pipelines_version2/envs/population-mapping-version2
```
To deactivate the environment (if you want to leave the conda environment)
```
conda deactivate
```
